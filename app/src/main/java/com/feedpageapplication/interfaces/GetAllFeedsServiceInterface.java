package com.feedpageapplication.interfaces;

import com.android.volley.VolleyError;
import com.feedpageapplication.beans.FeedListResponse;

/**
 * Created by keshav on 22/6/18.
 */

public interface GetAllFeedsServiceInterface {
    void getFeedsData(FeedListResponse feedListResponse);
    void error(VolleyError volleyError, String TAG);
}
