package com.feedpageapplication.services;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.VolleyError;
import com.feedpageapplication.beans.FeedListResponse;
import com.feedpageapplication.beans.Post;
import com.feedpageapplication.constants.Config;
import com.feedpageapplication.interfaces.GetAllFeedsServiceInterface;
import com.feedpageapplication.roomdb.AppDatabase;
import com.feedpageapplication.roomdb.FeedPost;
import com.feedpageapplication.utility.Logger;

public class RestApiCallService extends JobIntentService implements GetAllFeedsServiceInterface {
    static final int JOB_ID = 1000;
    final Handler mHandler = new Handler();
    LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, RestApiCallService.class, JOB_ID, work);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        showToast("Job Execution Started");
    }

    void showToast(final CharSequence text) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(RestApiCallService.this, text, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        String urls[]=intent.getStringArrayExtra("URLS");
        GetFeedsServiceApi getFeedsServiceApi=new GetFeedsServiceApi(this);
        getFeedsServiceApi.makeRequest(2,this);

        GetFeedsServiceApi getFeedsServiceApi3=new GetFeedsServiceApi(this);
        getFeedsServiceApi3.makeRequest(3,this);

    }


    @Override
    public void getFeedsData(FeedListResponse feedListResponse) {
      if(feedListResponse.getPosts().size()>0){
          for (Post post : feedListResponse.getPosts()) {
              FeedPost feedPost = new FeedPost();
              feedPost.setPostid(post.getId());
              feedPost.setThumbnailImage(post.getThumbnailImage());
              feedPost.setEventName(post.getEventName());
              feedPost.setEventDate(post.getEventDate());
              feedPost.setLikes(post.getLikes());
              feedPost.setViews(post.getViews());
              feedPost.setShares(post.getShares());
              feedPost.setPageno(feedListResponse.getPage());
             // new InsertQuestions().execute(feedPost);
              AppDatabase.getAppDatabase(this).userDao().insertAll(feedPost);
          }
           int count=AppDatabase.getAppDatabase(this).userDao().getQuestionCount();
           Logger.LogInfo("Count",count+"");
      }else{

      }
    }

    @Override
    public void error(VolleyError volleyError, String TAG) {

    }

    /*class InsertQuestions extends AsyncTask<QuestionAnswer, Void, Integer> {

        @Override
        protected Integer doInBackground(QuestionAnswer... strings) {

        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            Logger.LogInfo("RESULT", integer + "");
        }
    }*/


    @Override
    public void onDestroy() {
        super.onDestroy();
        showToast("Job Execution Finished");
        int count=AppDatabase.getAppDatabase(this).userDao().getQuestionCount();
        if(count>0){
            initBroadCast(true);
        }else{
            initBroadCast(false);
        }

    }

    public void initBroadCast(boolean isSucceeded){
        Intent intent=new Intent();
        intent.setAction(Config.JOB_SERVICE);
        if(isSucceeded) {
            intent.putExtra("KEY_SERVICE",Config.JOB_ACCOMPLISHED);
        }else{
            intent.putExtra("KEY_SERVICE",Config.JOB_FAILED);
        }
         localBroadcastManager.sendBroadcast(intent);
    }






}
