package com.feedpageapplication.services;

import android.content.Context;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.feedpageapplication.beans.FeedListResponse;
import com.feedpageapplication.constants.AppConstants;
import com.feedpageapplication.constants.Config;
import com.feedpageapplication.interfaces.GetAllFeedsServiceInterface;
import com.feedpageapplication.utility.Logger;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Map;


public class GetFeedsServiceApi {
    private static final String API_TAG = GetFeedsServiceApi.class.getSimpleName();
    private Context mContext;
    private GetAllFeedsServiceInterface getAllFeedsServiceInterface;

    public GetFeedsServiceApi(Context context) {
        this.mContext = context;
    }


    public void makeRequest(long pageNo,final GetAllFeedsServiceInterface getAllFeedsServiceInterface) {
        this.getAllFeedsServiceInterface=getAllFeedsServiceInterface;
        String feedUrl="";

        switch ((int)pageNo){
            case 1:
                feedUrl= Config.FEED_FIRST_PAGE_END_POINT;
                break;
            case 2:
                feedUrl= Config.FEED_SECOND_PAGE_END_POINT;
                break;
            case 3:
                feedUrl= Config.FEED_THIRD_PAGE_END_POINT;
                break;
        }


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,feedUrl, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Logger.LogError(API_TAG,response.toString());
                FeedListResponse feedListResponse = new Gson().fromJson(response.toString(), FeedListResponse.class);
                getAllFeedsServiceInterface.getFeedsData(feedListResponse);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Logger.LogError("TAG", error.getMessage());
                getAllFeedsServiceInterface.error(error,API_TAG);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return super.getParams();
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(AppConstants.CONNECTIONTIMEOUT,0,0));
        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(jsonObjectRequest);
    }
}
