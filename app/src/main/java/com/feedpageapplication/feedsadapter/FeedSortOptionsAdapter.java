package com.feedpageapplication.feedsadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.feedpageapplication.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keshav on 11/9/17.
 */

public class FeedSortOptionsAdapter extends BaseAdapter {
    private Context mContext;
    private List<String> diabetesTypes;
    private LayoutInflater layoutInflater;
    private ListView listView;

    public FeedSortOptionsAdapter(Context context, List<String> diabetesTypes, ListView listView) {
        mContext = context;
        this.diabetesTypes = diabetesTypes;
        layoutInflater = LayoutInflater.from(mContext);
        this.listView=listView;
    }


    @Override
    public int getCount() {
        return diabetesTypes.size();
    }

    @Override
    public Object getItem(int position) {
        return diabetesTypes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.layout_spinner_options_item, parent, false);
            holder.mTypeValue = (TextView)convertView.findViewById(R.id.diabetes_type_value_tv);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.mTypeValue.setText(diabetesTypes.get(position));
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             listView.getOnItemClickListener().onItemClick(listView, v, position, getItemId(position));
            }
        });
        return convertView;
    }

    static class ViewHolder {
        TextView mTypeValue;
    }

}