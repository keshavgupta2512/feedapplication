package com.feedpageapplication.feedsadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.feedpageapplication.R;
import com.feedpageapplication.beans.Post;
import com.feedpageapplication.constants.AppConstants;
import com.feedpageapplication.roomdb.FeedPost;
import com.feedpageapplication.utility.UtilityMethod;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FeedsListAdapter extends RecyclerView.Adapter<FeedsListAdapter.ViewHolder> {


    private Context context;
    private List<FeedPost> feedsList;
    private LayoutInflater mInflater;

    public FeedsListAdapter(Context context, List<FeedPost> feedsList) {
        this.context = context;
        this.feedsList = feedsList;
        this.mInflater = LayoutInflater.from(context);
    }

    public void updateList(List<FeedPost> list) {
        feedsList.clear();
        feedsList.addAll(list);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_user_name)
        TextView tvUserName;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.user_name)
        LinearLayout userName;
        @BindView(R.id.image_post_image)
        ImageView imagePostImage;

        @BindView(R.id.likes_list)
        TextView likesList;
        @BindView(R.id.share_count)
        TextView shareCount;
        @BindView(R.id.tv_comments_count)
        TextView tvCommentsCount;
        @BindView(R.id.likes_comments_shares_count)
        LinearLayout likesCommentsSharesCount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Glide.with(context).load(feedsList.get(position).getThumbnailImage()).into(holder.imagePostImage);
        holder.tvUserName.setText(feedsList.get(position).getEventName());
        holder.time.setText(UtilityMethod.getDateandTime(feedsList.get(position).getEventDate(), AppConstants.Y_M_D_H_M_S));
        holder.likesList.setText(feedsList.get(position).getLikes()+"");
        holder.shareCount.setText(feedsList.get(position).getShares()+"");
        holder.tvCommentsCount.setText(feedsList.get(position).getViews()+"");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_layout_feed_post, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return feedsList.size();
    }
}