
package com.feedpageapplication.beans;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeedListResponse {

    @SerializedName("posts")
    @Expose
    private List<Post> posts = null;
    @SerializedName("page")
    @Expose
    private long page;

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public long getPage() {
        return page;
    }

    public void setPage(long page) {
        this.page = page;
    }

}
