package com.feedpageapplication.utility;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.feedpageapplication.R;

import java.text.SimpleDateFormat;
import java.util.Date;


public class UtilityMethod {

    public static String getDateandTime(long timeStamp,String pattern) {
        String dateTime=null;
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat(pattern);
        Date date=new Date(timeStamp);
        dateTime=simpleDateFormat.format(date);
        return dateTime;
    }

    public static void showDialogIfUserUnregistered(String mesg,final Activity activity) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);
        builder.setTitle(R.string.Invalidsession);
        builder.setMessage(mesg);
        builder.setCancelable(false);
        String positiveText = activity.getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        androidx.appcompat.app.AlertDialog dialog = builder.create();
        // display dialog
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }
    }
    public static androidx.appcompat.app.AlertDialog showProgressBar(Context context, String text){
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context);
        androidx.appcompat.app.AlertDialog dialog = builder.create();
        View dialogLayout;
        dialogLayout = LayoutInflater.from(context).inflate(R.layout.custom_progress_bar, null);
        TextView text1=(TextView)dialogLayout.findViewById(R.id.loading_msg);
        text1.setText(text);
        dialog.setView(dialogLayout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }


    public static void toggleKeyboardVisibility(Context context) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static void activityEnterAnimation(Context context){
        ((Activity)context).overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }
    public static void activityExitAnimation(Context context){
        ((Activity)context).overridePendingTransition(R.anim.slide_from_left,R.anim.slide_to_right);
    }

    public static void hideKeyboard(final View view) {
        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager in = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }, 100);
    }


}
