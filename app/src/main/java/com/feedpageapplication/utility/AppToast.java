package com.feedpageapplication.utility;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;


public class AppToast {

    private static Toast toast;

    public static void showToast(Context mContext, String st, int length){ //"Toast toast" is declared in the class
        try{ toast.getView().isShown();     // true if visible
            toast.setText(st);
        } catch (Exception e) {         // invisible if exception
            toast = Toast.makeText(mContext, st,length);
            toast.show();
        }
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();  //finally display it
    }


}
