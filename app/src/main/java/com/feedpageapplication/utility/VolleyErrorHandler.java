package com.feedpageapplication.utility;

import android.app.Activity;
import android.content.Context;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.feedpageapplication.R;
import com.feedpageapplication.constants.AppConstants;
import com.feedpageapplication.constants.NetworkKeys;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by keshav on 20/6/18.
 */

public class VolleyErrorHandler {
    private static String TAG=VolleyErrorHandler.class.getSimpleName();
    public static String errorMesg(Context context, VolleyError volleyError)
    {
        String message="";
        NetworkResponse networkResponse = volleyError.networkResponse;
        if (networkResponse != null) {
            Logger.LogError(TAG, networkResponse.statusCode + "");
            String jsonError = new String(networkResponse.data);
            Logger.LogError(TAG, jsonError);
            if (jsonError.startsWith("{")) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonError);
                    if (jsonObject.has(NetworkKeys.KEY_STATUS)) {
                        if(jsonObject.optString(NetworkKeys.KEY_STATUS).equals(AppConstants.STATUSCODE_UNAUTHORIZED)){
                            if (jsonObject.has(NetworkKeys.KEY_MESSAGE)) {
                                message=jsonObject.optString(NetworkKeys.KEY_MESSAGE);
                                UtilityMethod.showDialogIfUserUnregistered(context.getString(R.string.valid_mesg_session_expiry),(Activity) context);
                                return "";
                            }
                        }else{
                            if (jsonObject.has(NetworkKeys.KEY_MESSAGE)) {
                                message=jsonObject.optString(NetworkKeys.KEY_MESSAGE);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                message=context.getString(R.string.alert_server_error);
            }

        } else {
            message=context.getString(R.string.alert_server_error);
        }
        return message;
    }

    public static String errorMesgString(Context context, String jsonError)
    {
        String message="";
        if(jsonError!=null){
            Logger.LogError(TAG, jsonError);
            if (jsonError.startsWith("{")) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonError);
                    if (jsonObject.has(NetworkKeys.KEY_STATUS)) {
                        if(jsonObject.optString(NetworkKeys.KEY_STATUS).equals(AppConstants.STATUSCODE_UNAUTHORIZED)){
                            if (jsonObject.has(NetworkKeys.KEY_MESSAGE)) {
                                message=jsonObject.optString(NetworkKeys.KEY_MESSAGE);
                                UtilityMethod.showDialogIfUserUnregistered(context.getString(R.string.valid_mesg_session_expiry),(Activity) context);
                                return "";
                            }
                        }else{
                            if (jsonObject.has(NetworkKeys.KEY_MESSAGE)) {
                                message=jsonObject.optString(NetworkKeys.KEY_MESSAGE);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                message=context.getString(R.string.alert_server_error);
            }
        }else{
            message=context.getString(R.string.alert_server_error);
        }
        return message;
    }
}
