package com.feedpageapplication.roomdb;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "FeedTable")
public class FeedPost {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "pageno")
    private long pageno;

    @ColumnInfo(name = "postid")
    private String postid;

    @ColumnInfo(name = "thumbnailImage")
    private String thumbnailImage;

    @ColumnInfo(name = "eventName")
    private String eventName;

    @ColumnInfo(name = "eventDate")
    private long eventDate;

    @ColumnInfo(name = "views")
    private long views;

    @ColumnInfo(name = "likes")
    private long likes;

    @ColumnInfo(name = "shares")
    private long shares;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPostid() {
        return postid;
    }

    public void setPostid(String postid) {
        this.postid = postid;
    }

    public String getThumbnailImage() {
        return thumbnailImage;
    }

    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public long getEventDate() {
        return eventDate;
    }

    public void setEventDate(long eventDate) {
        this.eventDate = eventDate;
    }

    public long getViews() {
        return views;
    }

    public void setViews(long views) {
        this.views = views;
    }

    public long getLikes() {
        return likes;
    }

    public void setLikes(long likes) {
        this.likes = likes;
    }

    public long getShares() {
        return shares;
    }

    public void setShares(long shares) {
        this.shares = shares;
    }

    public long getPageno() {
        return pageno;
    }

    public void setPageno(long pageno) {
        this.pageno = pageno;
    }
}
