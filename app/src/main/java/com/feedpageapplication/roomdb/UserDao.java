package com.feedpageapplication.roomdb;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface UserDao {

    @Query("Select Count(*) from FeedTable")
    int getQuestionCount();

    @Query("SELECT * FROM FeedTable WHERE pageno =:pageno")
    List<FeedPost> getAllPosts(long pageno);

    @Insert
    void insertAll(FeedPost... users);

    @Query("Delete from FeedTable")
    void deleteAll();

    @Query("SELECT * FROM FeedTable LIMIT :limit OFFSET :offset")
    List<FeedPost> loadAllUsersByPage(int limit,int offset);

    @Update
    void update(FeedPost user);


}
