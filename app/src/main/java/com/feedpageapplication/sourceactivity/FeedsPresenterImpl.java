package com.feedpageapplication.sourceactivity;

import android.content.Context;

import com.android.volley.VolleyError;
import com.feedpageapplication.R;
import com.feedpageapplication.beans.FeedListResponse;
import com.feedpageapplication.interfaces.GetAllFeedsServiceInterface;
import com.feedpageapplication.services.GetFeedsServiceApi;

public class FeedsPresenterImpl implements FeedsPresenter, GetAllFeedsServiceInterface {
    private Context context;
    private FeedsView feedsView;

    public FeedsPresenterImpl(Context context,FeedsView feedsView){
        this.context=context;
        this.feedsView=feedsView;
    }

    @Override
    public void onLoadFeed(long pageno) {
        if(feedsView.isNetworkConnected()){
            feedsView.showProgress();
            GetFeedsServiceApi getFeedsServiceApi=new GetFeedsServiceApi(context);
            getFeedsServiceApi.makeRequest(pageno,this);
        }else{
            feedsView.showToast(context.getString(R.string.app_name));
        }
    }

    @Override
    public void getFeedsData(FeedListResponse feedListResponse) {
         feedsView.hideProgress();
         if(feedListResponse!=null){
             if(feedListResponse.getPosts().size()>0){
               feedsView.postFeedsDataToScreen(feedListResponse);
             }else{
                 feedsView.errorInFeeds();
             }
         }
    }

    @Override
    public void error(VolleyError volleyError, String TAG) {
        feedsView.hideProgress();
        feedsView.errorInFeeds();
        /*String message = VolleyErrorHandler.errorMesg(context, volleyError);
        if(!message.equals(""))
            feedsView.showToast(message);*/

    }
}
