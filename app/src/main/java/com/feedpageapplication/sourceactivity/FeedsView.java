package com.feedpageapplication.sourceactivity;

import com.feedpageapplication.beans.FeedListResponse;

public interface FeedsView {
    void showProgress();
    void hideProgress();
    void showToast(String message);
    boolean isNetworkConnected();
    void postFeedsDataToScreen(FeedListResponse feedListResponse);
    void errorInFeeds();
}
