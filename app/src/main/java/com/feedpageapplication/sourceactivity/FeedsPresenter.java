package com.feedpageapplication.sourceactivity;

public interface FeedsPresenter {
    void onLoadFeed(long pageno);
}
