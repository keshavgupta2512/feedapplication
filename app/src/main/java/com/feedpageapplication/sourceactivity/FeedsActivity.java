package com.feedpageapplication.sourceactivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.feedpageapplication.R;
import com.feedpageapplication.beans.FeedListResponse;
import com.feedpageapplication.beans.Post;
import com.feedpageapplication.constants.Config;
import com.feedpageapplication.feedsadapter.FeedSortOptionsAdapter;
import com.feedpageapplication.feedsadapter.FeedsListAdapter;
import com.feedpageapplication.roomdb.AppDatabase;
import com.feedpageapplication.roomdb.FeedPost;
import com.feedpageapplication.services.RestApiCallService;
import com.feedpageapplication.utility.AppToast;
import com.feedpageapplication.utility.InternetConnection;
import com.feedpageapplication.utility.Logger;
import com.feedpageapplication.utility.UtilityMethod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeedsActivity extends AppCompatActivity implements FeedsView {
    @BindView(R.id.rlv_feeds)
    RecyclerView rlvFeeds;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tv_no_feeds_available)
    TextView tvNoFeedsAvailable;
    @BindView(R.id.iv_addmore)
    ImageView ivAddmore;

    private AppCompatActivity activity;
    private AlertDialog mProgressDialog;
    LinearLayoutManager linearLayoutManager;
    private FeedsPresenter feedsPresenter;
    private List<String> arrayListOptions=new ArrayList<>();
    long pageno = 1;
    private List<FeedPost> feedList;
    private FeedsListAdapter feedsListAdapter;
    LocalBroadcastManager localBroadcastManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initConfigs();
        arrayListOptions.add("Sort By Date");
        arrayListOptions.add("Sort By Like");
        arrayListOptions.add("Sort By View");
        arrayListOptions.add("Sort By Share");
        setUpFeedsView();
        if(InternetConnection.isInternetConnected(activity)){
            new DeleteAllFeeds().execute();
        }else{
            int count=AppDatabase.getAppDatabase(activity).userDao().getQuestionCount();
            if(count>0){
                pageno=1;
                feedList.clear();
                new GetPostTable().execute();
            }else{
                errorInFeeds();
            }
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageno=1;
                feedList.clear();
                int count=AppDatabase.getAppDatabase(activity).userDao().getQuestionCount();
                if(count>0){
                    new GetPostTable().execute();
                }else{
                    callFeedsApi();
                }

            }
        });
        registerReceiver(listener,new IntentFilter(Config.JOB_SERVICE));
    }

    private BroadcastReceiver listener = new BroadcastReceiver() {
        @Override
        public void onReceive( Context context, Intent intent ) {
          String jobstatus=intent.getStringExtra("KEY_SERVICE");
          if(jobstatus.equals(Config.JOB_ACCOMPLISHED)){
              /*rlvFeeds.setVisibility(View.VISIBLE);
              tvNoFeedsAvailable.setVisibility(View.GONE);
              new GetPostTable().execute();*/

          }else if(jobstatus.equals(Config.JOB_FAILED)){
              /*rlvFeeds.setVisibility(View.GONE);
              tvNoFeedsAvailable.setVisibility(View.VISIBLE);*/
          }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
       localBroadcastManager.unregisterReceiver(listener);
    }

    private void startService(){
        Intent mIntent = new Intent(this, RestApiCallService.class);
        mIntent.putExtra("URLS",new String[]{Config.FEED_SECOND_PAGE_END_POINT,Config.FEED_THIRD_PAGE_END_POINT});
        RestApiCallService.enqueueWork(this, mIntent);
    }

    private void callFeedsApi() {

        feedsPresenter.onLoadFeed(pageno);
    }

    private void initConfigs() {
        activity = FeedsActivity.this;
        localBroadcastManager = LocalBroadcastManager.getInstance(activity);
        feedList=new ArrayList<>();
        feedsPresenter = new FeedsPresenterImpl(this, this);
    }

    @Override
    public void showProgress() {
        hideProgress();
        mProgressDialog = UtilityMethod.showProgressBar(this, getString(R.string.toastLoading));
        mProgressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    private void setUpFeedsView() {
        linearLayoutManager = new LinearLayoutManager(activity);
        DividerItemDecoration verticalDecoration = new DividerItemDecoration(activity,
                DividerItemDecoration.VERTICAL);
        Drawable verticalDivider = ContextCompat.getDrawable(activity, R.drawable.divider_vertical_language);
        verticalDecoration.setDrawable(verticalDivider);
        rlvFeeds.addItemDecoration(verticalDecoration);
        rlvFeeds.setLayoutManager(linearLayoutManager);
        rlvFeeds.addOnScrollListener(scrollListener);
    }

    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            linearLayoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
            if(dy!=0){
                if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == feedList.size() - 1) {
                    new GetDbCount().execute();
                }
            }
        }
    };

    @Override
    public void showToast(String message) {
        AppToast.showToast(this, message, Toast.LENGTH_SHORT);
    }

    @Override
    public boolean isNetworkConnected() {
        return InternetConnection.isInternetConnected(this);
    }

    @Override
    public void postFeedsDataToScreen(FeedListResponse feedListResponse) {
        if(swipeRefreshLayout.isRefreshing()){
            swipeRefreshLayout.setRefreshing(false);
        }
        tvNoFeedsAvailable.setVisibility(View.GONE);
        rlvFeeds.setVisibility(View.VISIBLE);
        //pageno = feedListResponse.getPage();
        List<Post> currentFeedList=feedListResponse.getPosts();
        for (Post post : currentFeedList) {
            FeedPost feedPost = new FeedPost();
            feedPost.setPostid(post.getId());
            feedPost.setThumbnailImage(post.getThumbnailImage());
            feedPost.setEventName(post.getEventName());
            feedPost.setEventDate(post.getEventDate());
            feedPost.setLikes(post.getLikes());
            feedPost.setViews(post.getViews());
            feedPost.setShares(post.getShares());
            feedPost.setPageno(feedListResponse.getPage());
            new InsertQuestions().execute(feedPost);
        }
        new GetPostTable().execute();
        startService();

    }

    @Override
    public void errorInFeeds() {
        if(swipeRefreshLayout.isRefreshing()){
            swipeRefreshLayout.setRefreshing(false);
        }
        tvNoFeedsAvailable.setVisibility(View.VISIBLE);
        rlvFeeds.setVisibility(View.GONE);
    }

    @OnClick(R.id.iv_addmore)
    public void onLoadMore(){
        PopupWindow popupWindow=runpopup(this,arrayListOptions);
        popupWindow.showAsDropDown(ivAddmore);
    }

    public PopupWindow runpopup(Context context, List<String> arrayList) {
        final PopupWindow popupWindow = new PopupWindow();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popupView = inflater.inflate(R.layout.popupwindowlayout, null);
        ListView mListView = (ListView) popupView.findViewById(R.id.multiplechoicelistview);
        int w_dimen = getResources().getDimensionPixelSize(R.dimen.margin_150);
        popupWindow.setWidth(w_dimen);
        popupWindow.setFocusable(true);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        FeedSortOptionsAdapter feedSortOptionsAdapter = new FeedSortOptionsAdapter(context, arrayList, mListView);
        mListView.setAdapter(feedSortOptionsAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                popupWindow.dismiss();
                switch (i){
                    case 0:
                        sortByDate();
                        break;
                    case 1:
                        sortByLike();
                        break;
                    case 2:
                        sortByView();
                        break;
                    case 3:
                        sortByShare();
                        break;

                }
            }
        });
        popupWindow.setContentView(popupView);
        popupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        popupWindow.setContentView(popupView);
        return popupWindow;
    }

    private void sortByLike(){
        Collections.sort(feedList, (t0, t1) -> String.valueOf(t0.getLikes()).compareTo(String.valueOf(t1.getLikes())));
        feedsListAdapter.updateList(feedList);
    }

    private void sortByDate(){
        Collections.sort(feedList, (t0, t1) -> String.valueOf(t0.getEventDate()).compareTo(String.valueOf(t1.getEventDate())));
        feedsListAdapter.updateList(feedList);

    }

    private void sortByView(){
        Collections.sort(feedList, (t0, t1) -> String.valueOf(t0.getViews()).compareTo(String.valueOf(t1.getViews())));
        feedsListAdapter.updateList(feedList);

    }

    private void sortByShare(){
        Collections.sort(feedList, (t0, t1) -> String.valueOf(t0.getShares()).compareTo(String.valueOf(t1.getShares())));
        feedsListAdapter.updateList(feedList);
    }

    class InsertQuestions extends AsyncTask<FeedPost, Void, Integer> {

        @Override
        protected Integer doInBackground(FeedPost... strings) {
            AppDatabase.getAppDatabase(activity).userDao().insertAll(strings);
            return AppDatabase.getAppDatabase(activity).userDao().getQuestionCount();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            Logger.LogInfo("RESULT", integer + "");
        }
    }


    class GetDbCount extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... strings) {
            return AppDatabase.getAppDatabase(activity).userDao().getQuestionCount();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            Logger.LogInfo("RESULT", integer + "");
            if (feedList.size()<integer) {
                pageno = pageno + 1;
                new GetPostTable().execute();

            }
        }
    }

    class DeleteAllFeeds extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... strings) {
            AppDatabase.getAppDatabase(activity).userDao().deleteAll();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            callFeedsApi();
        }
    }

    class GetPostTable extends AsyncTask<Void, Void, List<FeedPost>> {

        @Override
        protected List<FeedPost> doInBackground(Void... voids) {
            List<FeedPost> feedsList = AppDatabase.getAppDatabase(activity).userDao().getAllPosts(pageno);
            return feedsList;
        }

        @Override
        protected void onPostExecute(List<FeedPost> feedPostList) {
            super.onPostExecute(feedPostList);
            if(swipeRefreshLayout.isRefreshing()){
                swipeRefreshLayout.setRefreshing(false);
            }
            feedList.addAll(feedPostList);
            feedsListAdapter = new FeedsListAdapter(activity,new ArrayList<FeedPost>());
            rlvFeeds.setAdapter(feedsListAdapter);
            feedsListAdapter.updateList(feedList);
        }

    }





}
