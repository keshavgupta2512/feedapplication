package com.feedpageapplication.constants;

public class AppConstants {
    public static final String STATUSCODE_UNAUTHORIZED="401";
    public static final int CONNECTIONTIMEOUT=30000;
    public static final String Y_M_D_H_M_S="yyyy-MM-dd HH:mm:ss";

}